//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2016/10/24
// Author: Jaroslav Sloup
// Author: Jaroslav Krivanek, Jiri Bittner, CTU Prague
// Edited: Jakub Hendrich, Daniel Meister, CTU Prague
//---------------------------------------------------------------------------

#include "sgl.h"
#include "Context.h"
#include <map>

using namespace std;
using namespace sgl;

/// Current error code.
static sglEErrorCode _libStatus = SGL_NO_ERROR;

static inline void setErrCode(sglEErrorCode c) {
    if (_libStatus == SGL_NO_ERROR)
        _libStatus = c;
}

// Context global data
static int contextId = 0;
static int lastContextId = 0;
static Context *context = nullptr;
static map<int, Context *> contexts;

bool checkContext() {
    if (!context || context->elementType != Context::TYPE_NONE) {
        setErrCode(SGL_INVALID_OPERATION);
        return false;
    }
    return true;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------
sglEErrorCode sglGetError() {
    sglEErrorCode ret = _libStatus;
    _libStatus = SGL_NO_ERROR;
    return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------
const char *sglGetErrorString(sglEErrorCode error) {
    static const char *errStrigTable[] =
        {
            "Operation succeeded",
            "Invalid argument(s) to a call",
            "Invalid enumeration argument(s) to a call",
            "Invalid call",
            "Quota of internal resources exceeded",
            "Internal library error",
            "Matrix stack overflow",
            "Matrix stack underflow",
            "Insufficient memory to finish the requested operation"
        };

    if ((int) error < (int) SGL_NO_ERROR || (int) error > (int) SGL_OUT_OF_MEMORY)
        return "Invalid value passed to sglGetErrorString()";

    return errStrigTable[(int) error];
}

//---------------------------------------------------------------------------
// Initialization functions
//---------------------------------------------------------------------------

void sglInit() {
    /* empty */
}

void sglFinish() {
    contexts.clear();
}

int sglCreateContext(int width, int height) {
    //FIXME undocumented error handling - remove?
    if (width <= 0 || height <= 0) {
        setErrCode(SGL_INVALID_VALUE);
        return 0;
    }

    auto ctx = new Context(width, height);
    contexts[++lastContextId] = ctx;
    return lastContextId;
}

void sglDestroyContext(int id) {
    auto ctx = contexts.find(id);
    if (ctx == contexts.end()) {
        setErrCode(SGL_INVALID_VALUE);
    } else if (ctx->first == contextId) {
        setErrCode(SGL_INVALID_OPERATION);
    } else {
        delete ctx->second;
        contexts.erase(ctx);
    }
}

void sglSetContext(int id) {
    auto ctx = contexts.find(id);
    if (ctx == contexts.end()) {
        setErrCode(SGL_INVALID_VALUE);
        contextId = 0;
        context = nullptr;
    } else {
        contextId = ctx->first;
        context = ctx->second;
    }
}

int sglGetContext() {
    if (!context)
        setErrCode(SGL_INVALID_OPERATION);
    return contextId;
}

float *sglGetColorBufferPointer() {
    return context ? reinterpret_cast<float *>(context->colorBuffer.data()) : nullptr;
}
//---------------------------------------------------------------------------
// Drawing functions
//---------------------------------------------------------------------------

void sglClearColor(float r, float g, float b, float /* alpha */) {
    if (checkContext())
        context->color = vec3(r, g, b);
}

void sglClear(unsigned what) {
    if (!checkContext()) return;
    if (what & ~(SGL_COLOR_BUFFER_BIT | SGL_DEPTH_BUFFER_BIT)) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    if (what & SGL_COLOR_BUFFER_BIT)
        context->clearDepthBuffer();
    if (what & SGL_COLOR_BUFFER_BIT)
        context->clearColorBuffer();
}

void sglBegin(sglEElementType mode) {
    if (!checkContext()) return;
    if (mode > 0 && mode < SGL_LAST_ELEMENT_TYPE)
        context->elementType = mode;
    else setErrCode(SGL_INVALID_ENUM);
}

void sglEnd() {
    if (!context || context->elementType == Context::TYPE_NONE)
        setErrCode(SGL_INVALID_OPERATION);
    else {
        context->render();
        context->vertexBuffer.clear();
        context->elementType = Context::TYPE_NONE;
    }
}

void sglVertex4f(float x, float y, float z, float w) {
    context->addVertex(x, y, z, w);
}

void sglVertex3f(float x, float y, float z) {
    context->addVertex(x, y, z, 1);
}

void sglVertex2f(float x, float y) {
    context->addVertex(x, y, 0, 1);
}

void sglCircle(float x, float y, float z, float radius) {
    if(!checkContext()) return;
    vec4 center = context->transformVertex(vec4(x,y,z,1));
    context->renderCircle(center, radius);
}

void sglEllipse(float x, float y, float z, float a, float b) {
    //TODO
}

void sglArc(float x, float y, float z, float radius, float from, float to) {
    //TODO
}

//---------------------------------------------------------------------------
// Transform functions
//---------------------------------------------------------------------------

void sglMatrixMode(sglEMatrixMode mode) {
    if (!checkContext()) return;
    if (mode != SGL_MODELVIEW && mode != SGL_PROJECTION)
        setErrCode(SGL_INVALID_ENUM);
    else context->matrixMode = mode;
}

void sglPushMatrix() {
    if (!checkContext()) return;
    auto& stack = context->matrixStack();
    mat4 mat = stack.top();
    stack.push(mat);
}

void sglPopMatrix() {
    if (!checkContext()) return;
    auto& stack = context->matrixStack();
    if (stack.size() == 1)
        setErrCode(SGL_STACK_UNDERFLOW);
    else stack.pop();
}

void sglLoadIdentity() {
    if (checkContext())
        context->matrixStack().top() = mat4();
}

void sglLoadMatrix(const float *matrix) {
    if (checkContext())
        context->matrixStack().top() = mat4(matrix);
}

void sglMultMatrix(const float *matrix) {
    if (checkContext())
        context->matrixMult(matrix);
}

void sglTranslate(float x, float y, float z) {
    if (!checkContext()) return;
    float m[16] =
        {1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 1, 0,
         x, y, z, 1};
    context->matrixMult(m);
}

void sglScale(float x, float y, float z) {
    if (!checkContext()) return;
    float m[16] =
        {x, 0, 0, 0,
         0, y, 0, 0,
         0, 0, z, 0,
         0, 0, 0, 1};
    context->matrixMult(m);
}

void sglRotate2D(float a, float x, float y) {
    if (!checkContext()) return;
    sglTranslate(x, y, 0);
    float m[16] =
        {   cos(a), sin(a), 0, 0,
           -sin(a), cos(a), 0, 0,
           0,    0,         1, 0,
           0,    0,         0, 1 };
    context->matrixMult(m);
    sglTranslate(-x, -y, 0);
}

void sglRotateY(float a) {
    if (!checkContext()) return;
    float m[16] =
        { cos(a), 0, sin(a), 0,
               0, 1,      0, 0,
         -sin(a), 0, cos(a), 0,
               0, 0,      0, 1};
    context->matrixMult(m);
}

void sglOrtho(float l, float r, float b, float t, float n, float f) {
    if (!checkContext()) return;
    float rl = 1 / (r - l);
    float tb = 1 / (t - b);
    float fn = 1 / (f - n);
    float m[16] =
        {     2*rl,         0,         0, 0,
                 0,      2*tb,         0, 0,
                 0,         0,     -2*fn, 0,
         -(r+l)*rl, -(t+b)*tb, -(f+n)*fn, 1};
    context->matrixMult(m);
}

void sglFrustum(float l, float r, float b, float t, float n, float f) {
    if (!checkContext()) return;
    float rl = 1 / (r - l);
    float tb = 1 / (t - b);
    float fn = 1 / (f - n);
    float n2 = 2*n;
    float m[16] =
        {   n2*rl,        0,         0,  0,
                0,    n2*tb,         0,  0,
         (r+l)*rl, (t+b)*tb, -(f+n)*fn, -1,
                0,        0, - n2*f*fn,  0};
    context->matrixMult(m);
}

void sglViewport(int x, int y, int width, int height) {
    if (!checkContext()) return;
    if (width <= 0 || height <= 0)
        setErrCode(SGL_INVALID_OPERATION);
    else {
        context->viewport = {x, y, width, height};
        ivec2 s = context->size;
        context->clip = {max(0, x), max(0, y), min(x + width, s.x), min(y + height, s.y)};
    }
}

//---------------------------------------------------------------------------
// Attribute functions
//---------------------------------------------------------------------------

void sglColor3f(float r, float g, float b) {
    if (checkContext())
        context->color = vec3(r, g, b);
}

void sglAreaMode(sglEAreaMode mode) {
    if (!checkContext()) return;
    if (mode >= SGL_POINT && mode <= SGL_FILL)
        context->areaMode = mode;
    else setErrCode(SGL_INVALID_ENUM);
}

void sglPointSize(float size) {
    if (!checkContext()) return;
    if (size < 1)
        setErrCode(SGL_INVALID_VALUE);
    context->pointSize = (int)size;
}

void sglEnable(sglEEnableFlags cap) {
    if (!checkContext()) return;
    if (cap != SGL_DEPTH_TEST)
        setErrCode(SGL_INVALID_ENUM);
    else context->depthTest = true;
}

void sglDisable(sglEEnableFlags cap) {
    if (!checkContext()) return;
    if (cap != SGL_DEPTH_TEST)
        setErrCode(SGL_INVALID_ENUM);
    else context->depthTest = false;
}

//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {}

void sglEndScene() {}

void sglSphere(const float x,
               const float y,
               const float z,
               const float radius) 
{}

void sglMaterial(const float r,
                 const float g,
                 const float b,
                 const float kd,
                 const float ks,
                 const float shine,
                 const float T,
                 const float ior) 
{}

void sglPointLight(const float x,
                   const float y,
                   const float z,
                   const float r,
                   const float g,
                   const float b) 
{}

void sglRayTraceScene() {}

void sglRasterizeScene() {}

void sglEnvironmentMap(const int width,
                       const int height,
                       float *texels)
{}

void sglEmissiveMaterial(const float r,
                         const float g,
                         const float b,
                         const float c0,
                         const float c1,
                         const float c2)
{}
