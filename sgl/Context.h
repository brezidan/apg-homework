#ifndef _SGL_CONTECT_H_
#define _SGL_CONTECT_H_

#include "sgl.h"
#include <stack>
#include <vector>
#include <cmath>

#include "Vector.h"
#include "Matrix.h"
#include "Operators.h"

namespace sgl {

    class Context {
    public:

        static const auto TYPE_NONE = sglEElementType(0);

        Context(int w, int h);

        //rendering

        std::vector<vec4> vertexBuffer;
        vec3 color = {1, 1, 1};
        vec3 clearColor;
        sglEElementType elementType = TYPE_NONE;
        sglEAreaMode areaMode = SGL_FILL;
        int pointSize = 1;
        bool depthTest = false;

        void render();
        void renderPoints();

        void renderLine(const vec4& v1, const vec4& v2);
        void renderTriangle(const vec4& v1, const vec4& v2, const vec4& v3);
        void renderCircle(const vec4& v, float r);

        inline void addVertex(float x, float y, float z, float w) {
            vertexBuffer.emplace_back(x, y, z, w);
        }

        // framebuffer

        ivec2 size;
        std::vector<vec3> colorBuffer;
        std::vector<float> depthBuffer;

        void clearColorBuffer();
        void clearDepthBuffer();

        inline void setPixel(int x, int y) {
            colorBuffer[y * size[0] + x] = color;
        }

        inline vec3* getPixel(int x, int y) {
            return &colorBuffer[y * size[0] + x];
        }

        // transformation & viewport

        mat4 transform; // final transform: projection * modelview
        bool transformDirty = true; // true if need to recompute transform

        ivec4 viewport; //x, y, width, height
        ivec4 clip;    //left x, left y, right x, right y
        sglEMatrixMode matrixMode = SGL_MODELVIEW;
        std::stack<mat4> matrixStacks[2]; // SGL_MODELVIEW or SGL_PROJECTION

        /**
         * return current matrix stack
         * @return stack
         */
        inline std::stack<mat4> &matrixStack() {
            transformDirty = true;
            return matrixStacks[matrixMode];
        }

        /**
         * multiplies top matrix of current stack
         * @param m matrix
         */
        void matrixMult(const float *m) {
            auto& M = matrixStack().top();
            mat4 mm = m;
            M = M * mm;
        }

        /**
         * updates transform matrix from modelview and projection
         */
        void updateTransform();

        /**
         * transforms vertex using modelview, projection and viewport:
         * 1. v = projection * modelview
         * 2. normalize x,y (perspective division)
         * 3. apply viewport transform to x,y
         * 4. floor x,y
         * (z,w coordinates are not normalized)
         * @param v vertex
         * @return transformed vertex
         */
        vec4 transformVertex(const vec4& v);
    };

}


#endif
