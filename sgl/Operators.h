#ifndef _SGL_OPERATORS_H_
#define _SGL_OPERATORS_H_

#include "Vector.h"
#include "Matrix.h"

namespace sgl {

    /**
     * M - matrix
     * V - vector
     * S - scalar
     */


    /* V operator S */

    // V *= S
    template<typename T, int N>
    inline Vector <T, N>& operator *= (Vector <T, N> &a, const T &b) {
        for (int i = 0; i < N; i++)
            a[i] *= b;
        return a;
    }

    // V /= S
    template<typename T, int N>
    inline Vector <T, N>& operator /= (Vector <T, N> &a, const T &b) {
        for (int i = 0; i < N; i++)
            a[i] /= b;
        return a;
    }

    // V += S
    template<typename T, int N>
    inline Vector <T, N>& operator += (Vector <T, N> &a, const T &b) {
        for (int i = 0; i < N; i++)
            a[i] += b;
        return a;
    }

    // V -= S
    template<typename T, int N>
    inline Vector <T, N>& operator -= (Vector <T, N> &a, const T &b) {
        for (int i = 0; i < N; i++)
            a[i] -= b;
        return a;
    }

    // V * S
    template<typename T, int N>
    inline Vector <T, N> operator * (const Vector <T, N> &a, const T &b) {
        Vector <T, N> ret = a;
        ret *= b;
        return ret;
    }

    // V / S
    template<typename T, int N>
    inline Vector <T, N> operator / (const Vector <T, N> &a, const T &b) {
        Vector <T, N> ret = a;
        ret /= b;
        return ret;
    }

    // V + S
    template<typename T, int N>
    inline Vector <T, N> operator + (const Vector <T, N> &a, const T &b) {
        Vector <T, N> ret = a;
        ret += b;
        return ret;
    }

    // V - S
    template<typename T, int N>
    inline Vector <T, N> operator - (const Vector <T, N> &a, const T &b) {
        Vector <T, N> ret = a;
        ret -= b;
        return ret;
    }


    /* V operator V */

    // V *= V
    template<typename T, int N>
    inline Vector <T, N>& operator *= (Vector <T, N> &a, const Vector <T, N> &b) {
        for (int i = 0; i < N; i++) a[i] *= b[i];
        return a;
    }

    // V /= V
    template<typename T, int N>
    inline Vector <T, N>& operator /= (Vector <T, N> &a, const Vector <T, N> &b) {
        for (int i = 0; i < N; i++) a[i] /= b[i];
        return a;
    }

    // V += V
    template<typename T, int N>
    inline Vector <T, N>& operator += (Vector <T, N> &a, const Vector <T, N> &b) {
        for (int i = 0; i < N; i++) a[i] += b[i];
        return a;
    }

    // V -= V
    template<typename T, int N>
    inline Vector <T, N>& operator -= (Vector <T, N> &a, const Vector <T, N> &b) {
        for (int i = 0; i < N; i++) a[i] -= b[i];
        return a;
    }

    // V * V
    template<typename T, int N>
    inline Vector <T, N> operator * (const Vector <T, N> &a, const Vector <T, N> &b) {
        Vector <T, N> ret = a;
        ret *= b;
        return ret;
    }

    // V / V
    template<typename T, int N>
    inline Vector <T, N> operator / (const Vector <T, N> &a, const Vector <T, N> &b) {
        Vector <T, N> ret = a;
        ret /= b;
        return ret;
    }

    // V + V
    template<typename T, int N>
    inline Vector <T, N> operator + (const Vector <T, N> &a, const Vector <T, N> &b) {
        Vector <T, N> ret = a;
        ret += b;
        return ret;
    }

    // V - V
    template<typename T, int N>
    inline Vector <T, N> operator - (const Vector <T, N> &a, const Vector <T, N> &b) {
        Vector <T, N> ret = a;
        ret -= b;
        return ret;
    }

    /* matrix operations */

    // M * V
    template<typename T, int N>
    Vector<T,N> operator * (const Matrix<T,N>& mat, const Vector<T,N>& vec) {
        Vector<T,N> ret;
        for(int c=0; c<N; c++)
            for(int r=0; r<N; r++)
                ret[r] += mat[c][r] * vec[c];
        return ret;
    }

    // M * M
    template<typename T, int N>
    Matrix<T,N> operator * (const Matrix<T,N>& mat1, const Matrix<T,N>& mat2) {
        Matrix<T,N> ret((T)0);
        for(int c=0; c<N; c++)
            for(int r=0; r<N; r++)
                for(int i=0; i<N; i++)
                    ret[c][r] += mat1[i][r] * mat2[c][i];
        return ret;
    }

}

#endif
