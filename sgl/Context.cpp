#include "Context.h"
#include <limits>
#include <algorithm>

using namespace std;

namespace sgl {

    Context::Context(int w, int h) {
        size = {w, h};
        colorBuffer.resize(w * h);
        depthBuffer.resize(w * h, std::numeric_limits<float>::max());
        colorBuffer.shrink_to_fit();
        depthBuffer.shrink_to_fit();
        matrixStacks[SGL_MODELVIEW].emplace();
        matrixStacks[SGL_PROJECTION].emplace();
    }

    void Context::clearColorBuffer() {
        int s = size.w * size.h;
        for (int i = 0; i < s; i++)
            colorBuffer[i] = clearColor;
    }

    void Context::clearDepthBuffer() {
        int s = size.w * size.h;
        for (int i = 0; i < s; i++)
            depthBuffer[i] = numeric_limits<float>::max();
    }

    void Context::updateTransform() {
        if (!transformDirty) return;
        transformDirty = false;
        transform = matrixStacks[SGL_PROJECTION].top() * matrixStacks[SGL_MODELVIEW].top();
    }

    vec4 Context::transformVertex(const vec4 &in) {
        vec4 v = transform * in;
        vec2 vp = (vec2)viewport.zw() / 2.0f;
        v.xy() = (vec2)viewport.xy() + vp + v.xy() / v.w * vp;
        v.x = floor(v.x);
        v.y = floor(v.y);
        return v;
    }

    void Context::render() {
        //transform all vertices
        updateTransform();
        for (vec4 &v : vertexBuffer)
            v = transformVertex(v);

        //remove unused vertices at end
        if (elementType == SGL_LINES && vertexBuffer.size() % 2)
            vertexBuffer.pop_back();
        else if (elementType == SGL_TRIANGLES && vertexBuffer.size() % 3 > 0) {
            vertexBuffer.pop_back();
            if (vertexBuffer.size() % 3 > 0)
                vertexBuffer.pop_back();
        }

        if (areaMode == SGL_POINT || elementType == SGL_POINTS) {
            renderPoints(); //just render everything as points
        } else {
            //SGL_LINES and SGL_FILL acts same for lines
            if (elementType == SGL_LINES) {
                for (int i = 0; i < vertexBuffer.size(); i += 2)
                    renderLine(vertexBuffer[i], vertexBuffer[i + 1]);

            } else if (elementType == SGL_LINE_STRIP) {
                for (int i = 1; i < vertexBuffer.size(); i++)
                    renderLine(vertexBuffer[i - 1], vertexBuffer[i]);

            } else if (elementType == SGL_LINE_LOOP || (elementType == SGL_POLYGON && areaMode == SGL_LINE)) {
                //polygon without fill acts as line loop
                for (int i = 1; i < vertexBuffer.size(); i++)
                    renderLine(vertexBuffer[i - 1], vertexBuffer[i]);
                renderLine(vertexBuffer.front(), vertexBuffer.back());

            } else if (areaMode == SGL_LINE && elementType == SGL_TRIANGLES) {
                for (int i = 0; i < vertexBuffer.size(); i += 3) {
                    renderLine(vertexBuffer[i], vertexBuffer[i + 1]);
                    renderLine(vertexBuffer[i + 1], vertexBuffer[i + 2]);
                    renderLine(vertexBuffer[i + 2], vertexBuffer[i]);
                }
            } else if (areaMode == SGL_FILL) {

                if (elementType == SGL_TRIANGLES)
                    for (int i = 0; i < vertexBuffer.size(); i += 3)
                        renderTriangle(vertexBuffer[i], vertexBuffer[i + 1], vertexBuffer[i + 2]);
                else if (elementType == SGL_POLYGON) {
                    /* TODO */
                }
            }
        }
    }

    void Context::renderPoints() {
        //simple implementation for point size one
        if (pointSize == 1) {
            for (auto &v : vertexBuffer) {
                int x = (int) v.x;
                int y = (int) v.y;
                if (x > clip.x && y > clip.y && x < clip.x2 && y < clip.y2)
                    setPixel(x, y);
            }
        } else {
            int s1 = pointSize / 2; //point half size
            int s2 = s1 + pointSize % 2; //for odd sizes

            for (auto &v : vertexBuffer) {
                int x = (int) v.x;
                int y = (int) v.y;

                //clip
                int x1 = max(clip.x, x - s1);
                int y1 = max(clip.y, y - s1);
                int x2 = min(clip.x2, x + s2);
                int y2 = min(clip.y2, y + s2);

                //faster implementation for bigger point size
                int ax = x2 - x1;
                int ay = size.w - ax;
                vec3 *px = getPixel(x1, y1);
                vec3 *ex = px + ax;
                vec3 *ey = px + size.w * (y2 - y1);
                while (px < ey) {
                    for (; px < ex; px++)
                        *px = color;
                    px += ay;
                    ex += size.w;
                }

                //original implementation
                /*for (int y = y1; y < y2; y++)
                    for (int x = x1; x < x2; x++)
                        setPixel(x, y);*/
            }
        }
    }

    void Context::renderLine(const vec4 &v1, const vec4 &v2) {

        //TODO replace with normal implementation

        //temporary, slow and incorrect implementation
        float step = 1.0f / max(abs(v1.x - v2.x), abs(v1.y - v2.y));
        for (float f = 0; f < 1.0f; f += step) {
            float F = 1 - f;
            int x = (int) (v1.x * f + v2.x * F);
            int y = (int) (v1.y * f + v2.y * F);
            if (x > clip.x && y > clip.y && x < clip.x2 && y < clip.y2)
                setPixel(x, y);
        }
    }

    void Context::renderTriangle(const vec4 &v1, const vec4 &v2, const vec4 &v3) {
        //TODO implement (use clip to check if is withing viewbuffer)
    }

    void Context::renderCircle(const vec4 &v, float r) {
        //TODO implement (use clip to check if is withing viewbuffer)
    }
}
