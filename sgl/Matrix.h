#ifndef _SGL_MATRIX_H_
#define _SGL_MATRIX_H_

#include "Vector.h"

namespace sgl {

    template<typename T, int N>
    struct Matrix {

        static const int size = N;
        static const int NN = N*N;
        using column = Vector<T, N>;

        /*
         * alternative implementation:
         * T data[N][N];
         */
        column data[N];

        inline T *array() {
            return reinterpret_cast<T *>(data);
        }

        inline column& operator[] (int i) {
            return data[i];
        }

        inline const column& operator[] (int i) const {
            return data[i];
        }

        Matrix() {
            auto arr = array();
            for (int i = 0; i < NN; i++)
                arr[i] = 0;
            for (int i = 0; i < N; i++)
                data[i][i] = 1;
        }

        Matrix(const T& s) {
            auto arr = array();
            for (int i = 0; i < NN; i++)
                arr[i] = s;
        }

        Matrix(const T *a) {
            auto arr = array();
            for (int i = 0; i < NN; i++)
                arr[i] = a[i];
        }
    };

    //add more if need
    using imat2 = Matrix<int, 2>;
    using imat3 = Matrix<int, 3>;
    using imat4 = Matrix<int, 4>;

    using mat2 = Matrix<float, 2>;
    using mat3 = Matrix<float, 3>;
    using mat4 = Matrix<float, 4>;
}

#endif
