#ifndef _SGL_VECTOR_H_
#define _SGL_VECTOR_H_

namespace sgl {

    template<typename T, int N>
    struct Vector;

    namespace internal {

        template<typename T, int N>
        inline T *array(Vector<T, N> *v) {
            return reinterpret_cast<T *>(v);
        }

        template<typename T, int N>
        inline const T *array(const Vector<T, N> *v) {
            return reinterpret_cast<const T *>(v);
        }

        template<typename T, int N>
        inline void fill(Vector<T, N> *v, const T &s) {
            for (int i = 0; i < N; i++)
                (*v)[i] = s;
        }

        template<typename T, int N>
        inline void init(Vector<T, N> *v, int i, const T &s) {
            (*v)[i] = s;
        }

        template<typename T, int N, typename... T2>
        inline void init(Vector<T, N> *v, int i, const T &s, const T2 &... args) {
            (*v)[i] = s;
            init<T, N>(v, i + 1, args ...);
        }

        template<typename T2, typename T, int N>
        Vector<T2, N> convert(const Vector<T, N> *v) {
            Vector<T2, N> ret;
            for(int i=0; i<N; i++)
                ret[i] = static_cast<T2>((*v)[i]);
            return ret;
        }

        template<int N2, typename T, int N>
        inline Vector<T, N2>& swizzle(Vector<T, N> *v, int i) {
            return *reinterpret_cast<Vector<T, N2>*>(array(v) + i);
        }

        template<int N2, typename T, int N>
        inline const Vector<T, N2>& swizzle(const Vector<T, N> *v, int i) {
            return *reinterpret_cast<const Vector<T, N2>*>(array(v) + i);
        }

        template<typename T, int N, typename F>
        inline Vector<T, N> apply(const Vector<T, N> *v, F f){
            Vector<T, N> ret;
            for (int i = 0; i < N; i++)
                ret[i] = f(v[i]);
            return ret;
        }
    }

    template<typename T, int N>
    struct Vector {
        static const int size = N;

        T data[N];

        Vector(const T& s) { internal::fill(this, s); }
        Vector() : Vector(0) {}

        // template constructor with N arguments
        template<typename... T2>
        Vector(const T& s, const T2 &... args) {
            internal::init<T,size>(this, 0, s, args...);
        }

        inline T &operator[](int i) {  return data[i]; }
        inline const T& operator[](int i) const { return data[i]; }

        template <typename F>
        Vector<T,size> apply(F f){ return internal::apply(this,f); }

        template <typename T2>
        operator Vector<T2,size> () { return internal::convert<T2>(this); }
    };

    /* specializations to add x, y, z ... */

    template<typename T>
    struct Vector<T,2> {
        static const int size = 2;

        union { T x; T u; T w; T width; };
        union { T y; T v; T h; T height; };

        Vector(const T& s) { internal::fill(this, s); }
        Vector() : Vector(0) {}

        template<typename... T2>
        Vector(const T& s, const T2 &... args) {
            internal::init<T,size>(this, 0, s, args...);
        }

        inline T &operator[](int i) { return internal::array(this)[i]; }
        inline const T& operator[](int i) const { return internal::array(this)[i]; }

        template <typename F>
        Vector<T,size> apply(F f){ return internal::apply(this,f); }

        template <typename T2>
        operator Vector<T2,size> () { return internal::convert<T2>(this); }
    };

    template<typename T>
    struct Vector<T,3> {
        static const int size = 3;

        union { T x; T r; };
        union { T y; T g; };
        union { T z; T b; };


        Vector(const T& s) { internal::fill(this, s); }
        Vector() : Vector(0) {}

        template<typename... T2>
        Vector(const T& s, const T2 &... args) {
            internal::init<T,size>(this, 0, s, args...);
        }

        inline T &operator[](int i) { return internal::array(this)[i]; }
        inline const T& operator[](int i) const { return internal::array(this)[i]; }

        Vector<T,2>& xy(){ return internal::swizzle<2>(this, 0); }
        Vector<T,2>& yz(){ return internal::swizzle<2>(this, 1); }

        const Vector<T,2>& xy() const { return internal::swizzle<2>(this, 0); }
        const Vector<T,2>& yz() const { return internal::swizzle<2>(this, 1); }

        template <typename F>
        Vector<T,size> apply(F f){ return internal::apply(this,f); }

        template <typename T2>
        operator Vector<T2,size> () { return internal::convert<T2>(this); }
    };

    template<typename T>
    struct Vector<T,4> {
        static const int size = 4;

        union { T x; T r; };
        union { T y; T g; };
        union { T z; T b; T x2; T width;  };
        union { T w; T a; T y2; T height; };

        Vector(const T& s) { internal::fill(this, s); }
        Vector() : Vector(0) {}

        template<typename... T2>
        Vector(const T& s, const T2 &... args) {
            internal::init<T,size>(this, 0, s, args...);
        }

        inline T &operator[](int i) { return internal::array(this)[i]; }
        inline const T& operator[](int i) const { return internal::array(this)[i]; }

        Vector<T,2>& xy(){ return internal::swizzle<2>(this, 0); }
        Vector<T,2>& yz(){ return internal::swizzle<2>(this, 1); }
        Vector<T,2>& zw(){ return internal::swizzle<2>(this, 2); }

        const Vector<T,2>& xy() const { return internal::swizzle<2>(this, 0); }
        const Vector<T,2>& yz() const { return internal::swizzle<2>(this, 1); }
        const Vector<T,2>& zw() const { return internal::swizzle<2>(this, 2); }

        const Vector<T,3>& xyz() const { return internal::swizzle<3>(this, 0); }
        const Vector<T,3>& yzw() const { return internal::swizzle<3>(this, 1); }

        template <typename F>
        Vector<T,size> apply(F f){ return internal::apply(this,f); }

        template <typename T2>
        operator Vector<T2,size> () { return internal::convert<T2>(this); }
    };

    //add more if need
    using ivec2 = Vector<int, 2>;
    using ivec3 = Vector<int, 3>;
    using ivec4 = Vector<int, 4>;

    using vec2 = Vector<float, 2>;
    using vec3 = Vector<float, 3>;
    using vec4 = Vector<float, 4>;
}

#endif
